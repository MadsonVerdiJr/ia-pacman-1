# Projeto PACMAN 1 - Madson Verdi Junior - v1.0.0

Este projeto busca responder as questões 1 até 8, disponíveis no arquivo `search_pt_BR.html`;

## Q1

Implemente o algoritmo de busca em profundidade (DFS) na função depthFirstSearch em search.py. Para fazer o seu algoritmo completar, escreva a versão da busca em grafo do DFS, que evita a expansão de quaisquer estados já visitados (seção 3.5 do livro).

O código deve encontrar rapidamente uma solução para:

```shell
python pacman.py -l tinyMaze -p SearchAgent

python pacman.py -l mediumMaze -p SearchAgent

python pacman.py -l bigMaze -z .5 -p SearchAgent
```

O tabuleiro do Pacman irá mostrar uma sobreposição dos estados explorados e da ordem em que foram exploradas (vermelho brilhante significa exploração mais cedo). A ordem de exploração é a que você esperaria? O Pacman realmente vai para todos os quadrados explorados em seu caminho para o objetivo?

*Dica: Se você usar uma pilha (Stack) como sua estrutura de dados, a solução encontrada pelo seu algoritmo DFS para o labirinto médio (mediumMaze) deve ter um comprimento de 130 (desde que você empurre sucessores para a franja na ordem fornecida por getSuccessors; você pode ter 244 se empurrá-los na ordem inversa). Será esta uma solução de menor custo? Se não, pense sobre o que a busca em profundidade está fazendo errado.*

## Q2

Implemente o algoritmo da busca em largura (BFS) na função breadthFirstSearch em search.py. Mais uma vez, escreva um algoritmo de busca em grafo que evita a expansão de quaisquer estados já visitados. Teste seu código da mesma forma que você fez para a busca em profundidade.

```shell
python pacman.py -l mediumMaze -p SearchAgent -a fn=bfs
python pacman.py -l bigMaze -p SearchAgent -a fn=bfs -z .5
```

A BFS encontra uma solução de menor custo? Se não, verifique a sua implementação.
Dica: Se o Pacman se mover muito lentamente, tente a opção --frameTime 0.

Nota: Caso você tenha escrito o seu código de pesquisa genericamente, ele deve funcionar igualmente bem para o problema de busca do quebra-cabeça "oito" (seção 3.2 do livro) sem quaisquer alterações.

```shell
python eightpuzzle.py
```




## Q3

Implemente o algoritmo de custo uniforme para busca em grafo na função uniformCostSearch em search.py. Encorajamos você a procurar em util.py por algumas estruturas de dados que podem ser úteis na sua implementação. Agora você deve observar um comportamento bem sucedido em todos os três layouts a seguir, onde os agentes abaixo são todos agentes UCS que diferem apenas na função de custo que usam (os agentes e as funções de custo estão escritos para você):

```shell
python pacman.py -l mediumMaze -p SearchAgent -a fn=ucs
python pacman.py -l mediumDottedMaze -p StayEastSearchAgent
python pacman.py -l mediumScaryMaze -p StayWestSearchAgent
```

Nota: Você deve obter custos de caminho muito baixos e muito altos para o StayEastSearchAgent e StayWestSearchAgent, respectivamente, devido às suas funções de custo exponenciais (veja searchAgents.py para detalhes).

## Q4

Implemente a busca em grafo A* na função vazia aStarSearch em search.py. O A* usa uma função heurística como um argumento. Heurísticas recebem dois argumentos: um estado no problema de busca (o principal argumento), e o problema em si (para informações de referência). A função heurística nullHeuristic em search.py ​​é um exemplo trivial.

Você pode testar sua implementação A* no problema original de encontrar um caminho através de um labirinto para uma posição fixa usando a heurística de distância de Manhattan (já implementada como manhattanHeuristic em searchAgents.py).

```shell
python pacman.py -l bigMaze -z .5 -p SearchAgent -a fn=astar,heuristic=manhattanHeuristic
```

Você deve observar que o A* encontra a solução ideal ligeiramente mais rápido do que a busca de custo uniforme (cerca de 549 vs 620 nós de busca expandidos em nossa implementação, mas os desempates na fila de prioridade podem fazer seus números serem ligeiramente diferentes). O que acontece em openMaze para as várias estratégias de busca?


## Q5

Implemente o problema de busca CornersProblem em searchAgents.py. Você terá que escolher uma representação dos estados que codifique todas as informações necessárias para detectar se todos os quatro cantos foram atingidos. Agora, o seu agente de busca deve resolver:

```shell
python pacman.py -l tinyCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
python pacman.py -l mediumCorners -p SearchAgent -a fn=bfs,prob=CornersProblem
```

Para receber o crédito integral, você precisa definir uma representação abstrata dos estados que não codifica informação irrelevante (como a posição de fantasmas, onde comida extra está, etc.). Em particular, não use um GameState do Pacman como um estado de busca. Seu código será muito, muito lento se você fizer isso (e também errado).

**Dica: As únicas partes do estado do jogo que você precisa referenciar na sua implementação são a posição inicial do Pacman e a localização dos quatro cantos.**

A nossa implementação de breadthFirstSearch expande um pouco menos de 2000 nós de busca em mediumCorners. No entanto, heurísticas (usadas na busca A*) podem reduzir a quantidade de busca necessária.


## Q6

Implemente uma heurística não-trivial e consistente para o CornersProblem em cornersHeuristic. Avaliação da nota: heurísticas inconsistentes não receberão nenhum crédito. 1 ponto para qualquer heurística não-trivial consistente. 1 ponto para a expansão de menos de 1.600 nós. 1 ponto para a expansão de menos de 1.200 nós. Expanda menos de 800 e você está indo muito bem!

`python pacman.py -l mediumCorners -p AStarCornersAgent -z 0.5`

Nota: `AStarCornersAgent` é um atalho para `-p SearchAgent -a fn=aStarSearch,prob=CornersProblem,heuristic=cornersHeuristic`.

__Admissibilidade__ vs __Consistência__: Lembre-se, heurísticas são apenas funções que recebem estados de busca e retornam números que estimam o custo para um objetivo mais próximo. Heurísticas mais eficazes retornarão valores mais próximos dos custos reais do objetivo. Para ser admissível, os valores da heurística devem ser limites inferiores do custo real do caminho mais curto para o próximo objetivo (e não negativos). Para ser consistente, a heurística deve, adicionalmente, garantir que se uma ação tem custo c, então tomar essa ação só pode causar uma queda na heurística de no máximo c.

Lembre-se que a admissibilidade não é suficiente para garantir a correção em buscas em grafos - é necessária a condição mais forte da consistência. No entanto, heurísticas admissíveis são geralmente também consistentes, especialmente se são derivadas de relaxações do problema. Por isso, é geralmente mais fácil começar inventando e criando heurísticas admissíveis. Uma vez que tiver uma heurística admissível que funciona bem, você pode verificar se ela é consistente também. A única maneira de garantir consistência é com uma prova. No entanto, a inconsistência pode geralmente ser detectada através da verificação de que, para cada nó que você expandir, nós sucessores são iguais ou maiores no valor de f. Além disso, se o UCS e o A* alguma vez retornarem caminhos de diferentes comprimentos, sua heurística é inconsistente. Essa coisa é complicada! Se você precisar de ajuda, não hesite em perguntar ao pessoal do curso.

Heurísticas não-triviais: As heurísticas triviais são aquelas que retornam zero sempre (UCS) e a heurística que calcula o custo verdadeiro de conclusão. O primeiro não vai lhe economizar nenhum tempo, enquanto o último irá estourar o tempo limite do avaliador automático. Você quer uma heurística que reduza o tempo de computação total, mas para esta tarefa o avaliador automático só irá verificar a contagem de nós (além de impor um limite de tempo razoável).

Além disso, qualquer heurística deve ser sempre não negativa, e deve retornar um valor de 0 em cada estado objetivo (tecnicamente este é um requisito da admissibilidade!). Vamos deduzir 1 ponto para qualquer heurística que retorna valores negativos, ou não se comporta adequadamente em estados objetivo.

## Q7

Preencha foodHeuristic em searchAgents.py com uma heurística consistente para o FoodSearchProblem. Teste o seu agente no mapa trickySearch:

`python pacman.py -l trickySearch -p AStarFoodSearchAgent`

Nosso agente UCS encontra a solução ótima em aproximadamente 13 segundos, explorando mais de 16.000 nós. Qualquer heurística não-trivial consistente receberá 1 ponto. Você também receberá os seguintes pontos adicionais, dependendo de quão poucos nós a sua heurística expandir.

_Lembre-se_: Se a sua heurística for inconsistente, você receberá nenhum crédito, por isso tome cuidado! Você consegue resolver mediumSearch em pouco tempo? Se sim, então estamos ou muito, muito impressionados, ou a sua heurística é inconsistente.

Vamos deduzir 1 ponto para qualquer heurística que retorna valores negativos, ou não retorna 0 em cada estado objetivo.


## Q8

Implemente a função `findPathToClosestDot` (encontra caminho para ponto mais próximo) em `searchAgents.py`. Nosso agente resolve esse labirinto (sub-otimamente!) em menos de um segundo com um custo de caminho de 350:

`python pacman.py -l bigSearch -p ClosestDotSearchAgent -z .5`

_Dica_: A forma mais rápida de completar `findPathToClosestDot` é preencher o problema `AnyFoodSearchProblem`, que está sem seu teste de objetivo. Depois, resolva esse problema com uma função de busca apropriada. A solução deve ser bem curta!

Seu agente `ClosestDotSearchAgent` nem sempre encontrará o caminho mais curto possível pelo labirinto. Na verdade, você pode fazer melhor se tentar.
